<?php

function numberMult($numero,$mult){
	if( ($numero%$mult) == 0 ){
		return true;
    }else {
		return false;
	}
}
	
for($i=1; $i<=100; $i++)
{
    if(numberMult($i,3) and numberMult($i,5)) {
	    echo 'FizzBuzz'.'<br>';
	}elseif(numberMult($i,3)) {
		echo 'Fizz'.'<br>';
	}elseif(numberMult($i,5)) {
		echo 'Buzz'.'<br>';
	}else {
	    echo $i.'<br>';
	}
}
?>