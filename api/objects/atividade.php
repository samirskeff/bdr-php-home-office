<?php
session_start();
class Atividade{
 
    // database connection and table name
    private $conn;
    private $table_name = "atividade";
 
    // object properties
    public $id;
    public $title;
    public $description;
    public $user_id;
    public $user_name;
    public $project;
    public $priority;
    public $status;
    public $created;
 
    // constructor with $db as database connection
    public function __construct($db){
        $this->conn = $db;
    }

    // read atividades
    function read(){
    
        // select all query
        $query = "SELECT
                    u.name as user_name, a.id, a.title, a.project, a.description, a.status, a.user_id, a.priority, a.created
                FROM
                    " . $this->table_name . " a
                    LEFT JOIN
                        user u
                            ON a.user_id = u.id
                ORDER BY
                    a.priority ASC, a.created DESC";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
        
        return $stmt;
    }
    // create 
    function create(){
    
        // query to insert record
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                    title = :title, project = :project, description = :description, user_id = :user_id,
                    status = :status, priority = :priority, created = :created";
    
        // prepare query
        $stmt = $this->conn->prepare($query);

        // sanitize
        $this->title=htmlspecialchars(strip_tags($this->title));
        $this->project=htmlspecialchars(strip_tags($this->project));
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->user_id=htmlspecialchars(strip_tags($this->user_id));
        $this->status=htmlspecialchars(strip_tags($this->status));
        $this->priority=htmlspecialchars(strip_tags($this->priority));
        $this->created=htmlspecialchars(strip_tags($this->created));
            
        // bind new values
        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':project', $this->project);
        $stmt->bindParam(':description', $this->description);
        $stmt->bindParam(':user_id', $this->user_id);
        $stmt->bindParam(':status', $this->status);
        $stmt->bindParam(':priority', $this->priority);
        $stmt->bindParam(":created", $this->created);
        
        if (!$stmt) {
            echo "\nPDO::errorInfo():\n";
            print_r($dbh->errorInfo());
        }

        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }

    function readOne(){
 
        // query to read single record
        $query = "SELECT
                    u.name as user_name, a.id, a.title, a.project, a.description, a.status, a.user_id,
                    a.status, a.priority, a.created
                FROM
                    " . $this->table_name . " a
                    LEFT JOIN
                        user u
                            ON a.user_id = u.id
                WHERE
                    a.id = :id
                LIMIT
                    0,1";
     
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
     
        // bind id of atividade to be updated
        $stmt->bindParam(':id', $this->id);
     
        if (!$stmt) {
            echo "\nPDO::errorInfo():\n";
            print_r($dbh->errorInfo());
        }
     

        // execute query
        $stmt->execute();
     
        // get retrieved row
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
      
     
        // set values to object properties
        $this->title = $row['title'];
        $this->project = $row['project'];
        $this->description = $row['description'];
        $this->user_id = $row['user_id'];
        $this->status = $row['status'];
        $this->priority = $row['priority'];
        $this->user_name = $row['user_name'];
    }

    // update
    function update(){
    
        // update query
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    title = :title,
                    project = :project,
                    description = :description,
                    user_id = :user_id,
                    status = :status,
                    priority = :priority
                WHERE
                    id = :id";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->title=htmlspecialchars(strip_tags($this->title));
        $this->project=htmlspecialchars(strip_tags($this->project));
        $this->description=htmlspecialchars(strip_tags($this->description));
        $this->user_id=htmlspecialchars(strip_tags($this->user_id));
        $this->status=htmlspecialchars(strip_tags($this->status));
        $this->priority=htmlspecialchars(strip_tags($this->priority));
        $this->id=htmlspecialchars(strip_tags($this->id));
    
        // bind new values
        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':project', $this->project);
        $stmt->bindParam(':description', $this->description);
        $stmt->bindParam(':user_id', $this->user_id);
        $stmt->bindParam(':status', $this->status);
        $stmt->bindParam(':priority', $this->priority);
        $stmt->bindParam(':id', $this->id);
        
        $msg = '';
       
        // execute the query
        if($stmt->execute()){
            return true;
        }else{
            $msg = $this->conn->errorInfo();
        }
        if($msg!=''){
            $_SESSION['msg']=$msg;
        }else{
            unset($_SESSION['msg']);
        }

        return false;
    }
    // delete the atividade
    function delete(){
    
        // delete query
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
    
        // prepare query
        $stmt = $this->conn->prepare($query);
    
        // sanitize
        $this->id=htmlspecialchars(strip_tags($this->id));
    
        // bind id of record to delete
        $stmt->bindParam(1, $this->id);
    
        // execute query
        if($stmt->execute()){
            return true;
        }
    
        return false;
        
    }
}