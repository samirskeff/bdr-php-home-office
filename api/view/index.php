<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
<head>
    <title>API Atividades</title>
    <link href="bootstrap/css/teste.css" rel="stylesheet" id="bootstrap-css">
    <script src="bootstrap/js/teste.js"></script>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />
</head>

<body class="container">
    <br />

    <div id="cf_source" class="panel panel-default">
        <div class="panel-heading">
          <div class="row">
                <div class="col-lg-6">
                    <h2> Lista de Atividades</h2>
                </div>
                <div class="col-lg-6 ">
                   <a link href="http://localhost/php-home/api/view/cadastro.php"> 
                     <button type="button" class="btn btn-success pull-right" style="margin-top: 20px !important;" >
                       Nova Atividade
                     </button>
                    </a>
                </div>        
          </div>  
        </div>
        <?php
            ini_set('user_agent', "PHP"); // github requires this
            $api = 'http://localhost';
            $url = $api . '/php-home/api/atividade/read.php';
            // make the request
            $response = file_get_contents($url);
            // check we got something back before decoding
            if(false !== $response) {
                $atividades = json_decode($response, true);
            }
           
        
        ?>
        <div class="panel-body source bg-success">
            <?php
                 foreach($atividades["records"] as $atividade ){
                     $id = $atividade['id'];
                     $title = $atividade['title'];
                     $status = $atividade['status'];
                     $desc_status = "";
                     switch ($status) {
                        case 1:
                            $desc_status = "Criado";
                            break;
                        case 2:
                            $desc_status = "Iniciado";
                            break;
                        case 3:
                            $desc_status = "Finalizado";
                            break;
                    }

            ?>
            <div id="atividade-<?=$id?>" title="atividade-<?=$id?>" class="col-lg-4" cf>
                <div class="draggable col-lg-12">
                <div class="panel panel-primary"> 
                    <div class="panel-heading" style="height: 80px;padding: 5px 5px 5px 5px;">
                        <div class="col-lg-12">
                            <a link href="http://localhost/php-home/api/view/atualizar.php?id=<?=$id?>">
                                <button type="button" class="btn btn-warning">
                                    <span class="glyphicon glyphicon-pencil"></span>
                                </button>    
                            </a>    
                            <a link href="http://localhost/php-home/api/view/excluir.php?id=<?=$id?>">
                                <button type="button" class="btn btn-danger">
                                    <span class="glyphicon glyphicon-remove"></span>
                                </button>    
                            </a>
                        </div>    
                        <div class="col-lg-12" style="margin-top: 10px;">
                            <span class="panel-title text"><?= $title?></span>                         
                        </div>    
                    </div> 
                    <div class="panel-body"> 
                       <?php
                        echo '<b>Projeto:</b> '. $atividade['project']."<br>";
                        echo '<b>Profissinal:</b> '. $atividade['user_name']."<br>";
                        echo '<b>Descrição:</b> '. $atividade['description']."<br>";
                        echo '<b>Prioridade:</b> '. $atividade['priority']."<br>";
                        echo '<b>Status:</b> '. $desc_status."<br>";
                        echo '<b>Criado:</b> '. date('d/m/Y',strtotime($atividade['created']))."<br>";
                       ?>
                    </div> 
                </div>
                </div>
            </div>
                 <?php }
                 ?>
           
        </div>
        
    </div>    
    <script src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
</body>
</html>