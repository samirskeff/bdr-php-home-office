<?php
    if(isset($_POST['title']) and $_POST['title']!=''){
        $title = $_POST['title'];
        $project = $_POST['project'];
        $description = $_POST['description'];
        $user_id = $_POST['user_id'];
        $status = $_POST['status'];
        $priority = $_POST['priority'];
        $created = date('Y-m-d H:i:s');
        

        $data = array("title" => $title, "project" => $project, "description" => $description,
        "user_id" => $user_id, "status" => $status, "priority" => $priority, "created" =>$created );
        $data_string = json_encode($data);                                                                                   
        
        $ch = curl_init('http://localhost/php-home/api/atividade/create.php');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json',
            'Content-Length: ' . strlen($data_string))
        );                                                                                                                   
        
        $result = curl_exec($ch);
        header("Location:http://localhost/php-home/api/view/");
        exit();

    }

?>


<link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!DOCTYPE html>
<html>
<head>
    <title>API Atividades</title>
    <link href="bootstrap/css/teste.css" rel="stylesheet" id="bootstrap-css">
    <script src="bootstrap/js/teste.js"></script>
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet" />
</head>

<body class="container">
    <br />

    <div id="cf_source" class="panel panel-default">
        <div class="panel-heading">
          <div class="row">
                <div class="col-lg-6">
                    <h2> Cadastro de Atividade</h2>
                </div>
                <div class="col-lg-6 ">
                    <a link href="http://localhost/php-home/api/view/index.php"> 
                        <button type="button" class="btn btn-success pull-right" style="margin-top: 20px !important;" >
                        Lista Atividade
                        </button>
                    </a>
                </div>        
          </div>  
        </div>
        <?php
            ini_set('user_agent', "PHP"); // github requires this
            $api = 'http://localhost';
            $url = $api . '/php-home/api/atividade/read.php';
            // make the request
            $response = file_get_contents($url);
            // check we got something back before decoding
            if(false !== $response) {
                $atividades = json_decode($response, true);
            }
           
        
        ?>
        <div class="panel-body source bg-success">
        <form action="cadastro.php" method="post">
            <div class="form-group">
                <label for="title">Titulo</label>
                <input type="text" name="title" class="form-control" id="title" placeholder="Titulo">
            </div>
            <div class="form-group">
                <label for="project">Projeto</label>
                <input type="text" name="project" class="form-control" id="project" placeholder="Projeto">
            </div>
            <div class="form-group">
                <label for="description">Descrição</label>
                <textarea class="form-control" rows="3" name="description"></textarea>
            </div>
            <div class="form-group">
                <label for="priority">Prioridade</label>
                <input type="text" name="priority" class="form-control" id="priority" placeholder="Prioridade" >
            </div>
            <div class="form-group">
                <label for="user_id">Profissional</label>
                <select class="form-control" name="user_id" id="user_id">
                    <option value="">Selecione</option>
                    <option value="1">Jairo Samir</option>
                    <option value="2">Jairo Ribeiro</option>
                    <option value="3">Samir Soares</option>
                </select>
            </div>
            <div class="form-group">
                <label for="status">Status</label>
                <select class="form-control" name="status" id="status">
                    <option value="">Selecione</option>
                    <option value="1">Criado</option>
                    <option value="2">Iniciado</option>
                    <option value="3">Finalizado</option>
                </select>
            </div>
            <button type="submit" class="btn btn-default">Enviar</button>
        </form>

        

          
        </div>
        
    </div>    
    <script src="https://code.jquery.com/ui/1.11.1/jquery-ui.min.js"></script>
</body>
</html>