-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 15-Maio-2019 às 03:36
-- Versão do servidor: 10.1.38-MariaDB
-- versão do PHP: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `api_phphome`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `atividade`
--

CREATE TABLE `atividade` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `project` varchar(150) NOT NULL,
  `description` varchar(255) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` int(2) NOT NULL,
  `priority` int(11) DEFAULT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `atividade`
--

INSERT INTO `atividade` (`id`, `title`, `project`, `description`, `user_id`, `status`, `priority`, `created`, `modified`) VALUES
(3, 'Cadastro de Estudante', 'WebEscola', 'Efetuar e manter o cadastro de estudante', 1, 1, 3, '2019-05-14 00:00:00', '2019-05-14 19:51:09'),
(4, 'Cadastro de Turma', 'WebEscola', 'Efetuar e manter o cadastro de turma', 2, 1, 5, '2019-05-14 00:00:00', '2019-05-14 19:50:58'),
(5, 'Cadastro de Turno/Periodo TESTE', 'WebEscola', 'Efetuar e manter o cadastro de turno/periodo', 2, 1, 5, '2019-05-14 21:56:02', '2019-05-14 23:52:53'),
(6, 'Cadastro de Materia/Serie', 'WebEscola', 'Efetuar e manter a avliação da materia/seria por aluno e professor', 2, 1, 2, '2019-05-14 22:13:26', '2019-05-14 20:13:26'),
(7, 'teste no app ff', 'TESTE', 'esse é um teste feito pela web pro apo', 1, 1, 1, '2019-05-15 01:06:30', '2019-05-15 00:20:07');

-- --------------------------------------------------------

--
-- Estrutura da tabela `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(250) NOT NULL,
  `depatament` varchar(150) NOT NULL,
  `created` datetime NOT NULL,
  `modified` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `user`
--

INSERT INTO `user` (`id`, `name`, `depatament`, `created`, `modified`) VALUES
(1, 'Jairo Samir', 'Analista de Sistema', '2019-05-14 00:00:00', '2019-05-14 03:00:00'),
(2, 'Jairo Ribeiro', 'Analista de Sistema', '2019-05-14 00:00:00', '2019-05-14 17:37:24'),
(3, 'Samir Soares', 'Administrador', '2019-05-14 00:00:00', '2019-05-14 03:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `atividade`
--
ALTER TABLE `atividade`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `atividade`
--
ALTER TABLE `atividade`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- Constraints for dumped tables
--

--
-- Limitadores para a tabela `atividade`
--
ALTER TABLE `atividade`
  ADD CONSTRAINT `user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
