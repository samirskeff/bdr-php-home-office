<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/atividade.php';
 
// instantiate database and atividade object
$database = new Database();
$db = $database->getConnection();
 
// initialize object
$atividade = new Atividade($db);

// query atividades
$stmt = $atividade->read();
$num = $stmt->rowCount();
 

// check if more than 0 record found
if($num>0){
 
    // atividades array
    $atividades_arr=array();
    $atividades_arr["records"]=array();
 
    // retrieve our table contents
    // fetch() is faster than fetchAll()
    while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        // extract row
        // this will make $row['name'] to
        // just $name only
        extract($row);
 
        $atividade_item=array(
            "id" => $id,
            "title" => html_entity_decode($title),
            "project" => html_entity_decode($project),
            "description" => html_entity_decode($description),
            "user_id" => $user_id,
            "user_name" => $user_name,
            "status" => $status,
            "priority" => $priority,
            "created" => $created
        );
 
        array_push($atividades_arr["records"], $atividade_item);
    }
 
    // set response code - 200 OK
    http_response_code(200);
 
    // show products data in json format
    echo json_encode($atividades_arr);
}else{
 
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user no products found
    echo json_encode(
        array("message" => "Não foi encontrado a Atividade.")
    );
}
 
?>