<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: access");
header("Access-Control-Allow-Methods: GET");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json');
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/atividade.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare atividade object
$atividade = new Atividade($db);
 
// set ID property of record to read
$atividade->id = isset($_GET['id']) ? $_GET['id'] : die();

// read the details of atividade to be edited
$stmt = $atividade->readOne();
//$num = $stmt->rowCount();

 
if($atividade->title!=null){
    // create array
    $atividade_arr = array(
        "id" =>  $atividade->id,
        "title" => $atividade->title,
        "description" => $atividade->description,
        "project" => $atividade->project,
        "user_id" => $atividade->user_id,
        "user_name" => $atividade->user_name,
        "status" => $atividade->status,
        "priority" => $atividade->priority
         
    );
 
    // set response code - 200 OK
    http_response_code(200);
 
    // make it json format
    echo json_encode($atividade_arr);
}
 
else{
    // set response code - 404 Not found
    http_response_code(404);
 
    // tell the user atividade does not exist
    echo json_encode(array("message" => "Atividade não existe."));
}
?>
