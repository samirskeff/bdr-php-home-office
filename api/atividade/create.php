<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// get database connection
include_once '../config/database.php';
 
// instantiate atividade object
include_once '../objects/atividade.php';
 
$database = new Database();
$db = $database->getConnection();
 
$atividade = new Atividade($db);
 
// get posted data
$data = json_decode(file_get_contents("php://input"));

// make sure data is not empty
if(
    ($data->title != '' ) &&
    ($data->project != '' ) &&
    ($data->description != '' ) &&
    ($data->user_id != '' ) &&
    ($data->status != '' )
){
 
    // set atividade property values
    $atividade->title = $data->title;
    $atividade->project = $data->project;
    $atividade->description = $data->description;
    $atividade->user_id = $data->user_id;
    $atividade->status = $data->status;   
    $atividade->priority = $data->priority; 
    $atividade->created = date('Y-m-d H:i:s');
 
    // create the atividade
    if($atividade->create()){
 
        // set response code - 201 created
        http_response_code(201);
 
        // tell the user
        echo json_encode(array("message" => "Atividade criado com sucesso!"));
    }
 
    // if unable to create the atividade, tell the user
    else{
 
        // set response code - 503 service unavailable
        http_response_code(503);
 
        // tell the user
        echo json_encode(array("message" => "Erro na criação da atividade."));
    }
}
 
// tell the user data is incomplete
else{
 
    // set response code - 400 bad request
    http_response_code(400);
 
    // tell the user
    echo json_encode(array("message" => "Erro na criação da atividade. Dados incompletos."));
}
?>