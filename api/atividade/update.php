<?php
// required headers
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
 
// include database and object files
include_once '../config/database.php';
include_once '../objects/atividade.php';
 
// get database connection
$database = new Database();
$db = $database->getConnection();
 
// prepare atividade object
$atividade = new Atividade($db);
 
// get id of atividade to be edited
$data = json_decode(file_get_contents("php://input"));
 
// set ID property of atividade to be edited
$atividade->id = $data->id;
 
// set atividade property values
$atividade->title = $data->title;
$atividade->project = $data->project;
$atividade->description = $data->description;
$atividade->user_id = $data->user_id;
$atividade->status = $data->status;
$atividade->priority = $data->priority;
 
// update the atividade
if($atividade->update()){
 
    // set response code - 200 ok
    http_response_code(200);
 
    // tell the user
    echo json_encode(array("message" => "Atividade atualizado."));
}
 
// if unable to update the atividade, tell the user
else{
 
    // set response code - 503 service unavailable
    http_response_code(503);
 
    // tell the user
    echo json_encode(array("message" => "Erro ao atualizar atividade.","erro"=>var_dump($_SESSION['msg'])));
}
?>